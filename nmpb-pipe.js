var nmpb_id        = "561738578bfc6a71d8c9a907";
var output_channel = "lobby";
var input_channel  = "NMPB Autoplayer (Troll Heaven2)";
var my_name        = "\u034f";
var mppUri         = "ws://www.multiplayerpiano.com:443";
var proxyUri       = "http://104.196.170.177:3128"

var Client = require('./Client');
var proxyClient = require('./proxyClient');

var client1 = new Client(mppUri);
client1.setChannel(output_channel);
var client2 = new Client(mppUri);
client2.setChannel(input_channel);
var client3 = new proxyClient(mppUri, proxyUri);
client3.setChannel(input_channel);

function startClients() {
        client1.start();
        setTimeout(function(){
                client2.start();
        }, 2000);
        client3.start();
}
startClients();

client1.on("connect", function(){
	client1.sendArray([{m:'userset', set:{name:my_name}}]);
});
client3.on("connect", function(){
	client3.sendArray([{m:'userset', set:{name:my_name}}]);
});

client1.on("disconnect", function(){
	client1.stop();
	client2.stop();
	client3.stop();
	setTimeout(()=>{
		startClients();
	}, 5000);
});

//var st;
client2.on('n', msg => {
    if (client2.findParticipantById(msg.p)._id != nmpb_id) return;
    client1.sendArray([msg]);
	//if (st) clearTimeout(st);
	//st = setTimeout(function(){
	//	client2.sendArray([{m:'a', message:"/r"}]);
	//}, 5000);
});

var t;
client3.on('m', msg => {
	if (client2.findParticipantById(msg.id)._id != nmpb_id) return;
	if (msg.y == 15 && msg.x >= 8 && msg.x <= 88) {
		if (t) clearTimeout(t);
		t = setTimeout(function(){
			client3.sendArray([{m:'a', message:"/r"}]);
		}, 10000);
	}
});







/*if (input_channel.endsWith("(Turns)")) {
    client2.on('a', msg => {
        if (msg.p._id != nmpb_id) return;
        if (msg.a == `It is now ${client2.getOwnParticipant().name}'s turn. You have 30 seconds to pick a track.`) {
            client2.sendArray([{m:'a', message:"/r"}]);
        }
    });
} else { // no turns
    let cst;
    client2.on('a', msg => {
		if (msg.p._id != nmpb_id) return;
		if (msg.a.startsWith("Reading file Id") || msg.a.startsWith("Tempo")) { //todo tempo time is total time regardless of current position soo...
			if (msg.a.endsWith('.')) msg.a = msg.a.slice(0,-1);
			var t = msg.a.slice(-5, -1).split(':');
			var ms = (t[0]*60000)+(t[1]*1000);
			if (cst) clearTimeout(cst);
			cst = setTimeout(function(){
				client2.sendArray([{m:'a', message:"/r"}]);
			}, ms);
		}
    });
}*/

//setInterval(function(){
//        client2.sendArray([{m:'a', message:"/r"}]);
//}, 1000*60*30) // just in case

